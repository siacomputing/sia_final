package Sia.Controller;

import Sia.Model.*;
import Sia.Model.Module;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class Main extends Application {
    //Dashboard View Elements
    static String currentUser = "";
    static String[] params;
    static int lastLoaded = 5;
    static String[] name;


    @FXML
    Text dashboardWelcomeText;
    @FXML
    Text dashboardTasksRemaining;
    @FXML
    Text dashboardCurrentWeek;
    @FXML
    TabPane dashboardTabPane;
    @FXML
    VBox dashboardTasksRemainingAP;
    @FXML
    VBox dashboardDeadlinesVBox;
    @FXML
    Text dashboardDeadlinesText;
    @FXML
    Text dashboardMilestoneText;
    @FXML
    VBox dashboardMilestonesVBox;

    //Tasks tab
    @FXML
    Text tasksTasksRemaining;
    @FXML
    VBox tasksTaskVBox;
    @FXML
    VBox tasksModulesVBox;

    //Assessments tab
    @FXML
    Text assessmentsAssessmentsRemaining;
    @FXML
    VBox dashboardAssessmentsVBox;

    //Milestones tab
    @FXML
    Text milestonesMilestonesRemaining;
    @FXML
    VBox milestonesMilestoneVB;

    //Login View Elements
    @FXML
    Button loginButton;
    @FXML
    Button loginRegisterButton;
    @FXML
    TextField loginUsername;
    @FXML
    TextField loginPassword;
    @FXML
    Text loginConPasswordLabel;
    @FXML
    TextField loginConPassword;
    @FXML
    Label loginInfo;
    @FXML
    DatePicker loginDateFrom;
    @FXML
    Text loginDateFromLabel;
    @FXML
    DatePicker loginDateTo;
    @FXML
    Text loginDateToLabel;
    @FXML
    Text loginForenameLabel;
    @FXML
    Text loginSurnameLabel;
    @FXML
    TextField loginForename;
    @FXML
    TextField loginSurname;

    //New activity dialog window elements
    @FXML
    ComboBox dialogTaskCB;
    @FXML
    Button dialogActivityButton;

    //New task dialog window elements
    @FXML
    Button dialogTaskButton;
    @FXML
    ComboBox dialogTaskModuleCode;
    @FXML
    TextArea dialogTaskDesc;
    @FXML
    TextArea dialogTaskNotes;
    @FXML
    ComboBox dialogTaskType;
    @FXML
    RadioButton dialogTaskCWRadio;
    @FXML
    ComboBox dialogTaskAssessmentCombo;
    @FXML
    DatePicker dialogTaskDate;

    //New milestone dialog window elements
    @FXML
    Button dialogMilestoneButton;
    @FXML
    TextArea dialogMilestoneNotes;
    @FXML
    TextArea dialogMilestoneDescription;
    @FXML
    TextArea dialogMilestoneTitle;
    @FXML
    ComboBox dialogMilestoneAssessment;
    @FXML
    RadioButton dialogMilestoneCW;
    @FXML
    ComboBox dialogMilestoneModuleCode;

    //new assessment dialog window elements
    @FXML
    Button dialogAssessmentButton;
    @FXML
    RadioButton dialogAssessmentRadioCW;
    @FXML
    RadioButton dialogAssessmentRadioExam;
    @FXML
    TextField dialogAssessmentCWTitle;
    @FXML
    DatePicker dialogAssessmentDateSet;
    @FXML
    Label dialogAssessmentDateDueLabel;
    @FXML
    TextField dialogAssessmentModuleCode;
    @FXML
    DatePicker dialogAssessmentDateDue;
    @FXML
    TextField dialogAssessmentValue;
    @FXML
    TextField dialogAssessmentTasks;


    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Sia/View/login.fxml"));
        Parent root = loader.load();
        params = getParameters().getRaw().toArray(new String[0]);
        primaryStage.setTitle("Log in");
        primaryStage.setScene(new Scene(root, 660, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);

    }

    public void dashboardNewTaskButton(ActionEvent e){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/Sia/View/dialogTask.fxml"));
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Create new Task");
            primaryStage.setScene(new Scene(root, 420, 470));
            primaryStage.show();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public void dashboardNewMilestoneButton(ActionEvent e){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/Sia/View/dialogMilestone.fxml"));
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Create new milestone");
            primaryStage.setScene(new Scene(root, 420, 470));
            primaryStage.show();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public void dashboardFillInfo(Event e) {
        switch (dashboardTabPane.getSelectionModel().getSelectedIndex()) {
            case 1:
                if (lastLoaded != 1) {
                    tasksTasksRemaining.setText(String.format("Tasks remaining: %d", Student.GetTaskCountFromDB(currentUser)));
                    ArrayList<Task> tasks = Student.GetTasksFromDB(currentUser);
                    ArrayList<VBox> taskCards = new ArrayList<>();
                    for (Task task : tasks) {
                        VBox card = new VBox();

                        Label taskName = new Label(task.description);
                        Label taskType = new Label(task.taskType.toString());
                        Label dueDate = new Label(task.dueDate);
                        Button gantt = new Button("View as gantt");
                        gantt.setStyle(String.valueOf(task.id));
                        Separator separator = new Separator(Orientation.HORIZONTAL);

                        EventHandler<ActionEvent> actionEventHandler = new EventHandler<>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                Button pressed = (Button) actionEvent.getSource();
                                showGantt(pressed.getStyle());
                            }
                        };
                        gantt.setOnAction(actionEventHandler);

                        card.getChildren().addAll(taskName, taskType, dueDate, gantt, separator);

                        taskCards.add(card);
                    }
                    ArrayList<Integer> Modules = Module.getListOfEnrolledModule(currentUser);
                    ArrayList<VBox> ModuleCards = new ArrayList<>();
                    for (Integer md : Modules) {
                        VBox card = new VBox();
                        int timeSpent = 0;

                        ArrayList<Integer> Tasks = new ArrayList<>();
                        for (Integer cw : Coursework.getListOfTasks(md)) {
                            Tasks.add(cw);
                        }
                        for (Integer exam : Exam.getListOfTasks(md)) {
                            Tasks.add(exam);
                        }

                        for (Integer taskID : Tasks) {
                            ArrayList<Integer> Activities = Task.getListOfActivities(taskID);

                            for (Integer activityID : Activities) {
                                timeSpent += Activity.getTimeTaken(activityID);

                            }
                        }

                        String formattedTime = String.format("%s hours, %s minutes, %s seconds",
                                timeSpent / 3600, (timeSpent % 3600) / 60, timeSpent % 60);

                        Label title = new Label(Module.getModuleName(md));
                        Label TimeSpent = new Label(formattedTime);
                        Separator separator = new Separator(Orientation.HORIZONTAL);

                        card.getChildren().addAll(title, TimeSpent, separator);
                        ModuleCards.add(card);
                    }

                    tasksTaskVBox.getChildren().clear();
                    tasksTaskVBox.getChildren().addAll(taskCards);

                    tasksModulesVBox.getChildren().clear();
                    tasksModulesVBox.getChildren().addAll(ModuleCards);

                }
                break;

            case 2:
                if (lastLoaded != 2) {
                    ArrayList<VBox> cardsToAdd = new ArrayList<>();
                    ArrayList<Assessment> Assessments = Student.GetAssessmentsFromDB(currentUser);
                    for (Assessment assesment : Assessments) {
                        VBox card = new VBox();
                        String[] info = assesment.toString().split(",");


                        if (assesment.getClass().getName().equals("Sia.Model.Coursework") && info[4] != null ) {
                            Label moduleName = new Label(info[4]);
                            Label taskName = new Label(info[0]);
                            Label value = new Label(info[1]);
                            Label dateSet = new Label(info[2]);
                            Label dateDue = new Label(info[3]);
                            Separator separator = new Separator(Orientation.HORIZONTAL);
                            card.getChildren().addAll(moduleName, taskName, value, dateSet, dateDue, separator);
                        } else if (info[2] != null){
                            Label module = new Label(info[2]);
                            Label value = new Label(info[0]);
                            Label dateDue = new Label(info[1]);
                            Separator separator = new Separator(Orientation.HORIZONTAL);
                            card.getChildren().addAll(module, value, dateDue, separator);

                        }
                        cardsToAdd.add(card);
                    }
                    dashboardAssessmentsVBox.getChildren().clear();
                    dashboardAssessmentsVBox.getChildren().addAll(cardsToAdd);
                    assessmentsAssessmentsRemaining.setText(String.format("Assessments remaining: %d", dashboardAssessmentsVBox.getChildren().size()));
                }
                break;

            case 3:
                if (lastLoaded != 3) {
                    ArrayList<VBox> cardsToAdd = new ArrayList<>();
                    Map<String, long[]> ms = Milestone.GetMilestonesFromDBForUser(currentUser);
                    for (Map.Entry<String, long[]> milestone : ms.entrySet()) {
                        VBox card = new VBox();

                        String milestoneDate = "";

                        //Check if it is coursework or not
                        if (milestone.getValue()[0] == 0) {
                            //is exam
                            milestoneDate = Exam.getDateFromDB(Long.toString(milestone.getValue()[1]));

                        } else {
                            //is coursework
                            milestoneDate = Coursework.getDateFromDB(Long.toString(milestone.getValue()[0]));
                        }
                        String[] moreData = Milestone.MilestoneData(milestone.getKey());

                        Label title = new Label(milestone.getKey());
                        Label dueDate = new Label(milestoneDate);
                        Label desc = new Label(moreData[0]);
                        Label notes = new Label(moreData[1]);


                        CheckBox complete = new CheckBox("Is complete?");
                        complete.setSelected(Milestone.isMilestoneComplete(milestone.getKey()));
                        complete.setId(milestone.getKey());


                        EventHandler<ActionEvent> actionEventHandler = new EventHandler<>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                CheckBox used = (CheckBox) actionEvent.getSource();
                                Milestone.updateMilestone(used.getId(),used.isSelected());
                            }
                        };
                        complete.setOnAction(actionEventHandler);
                        Separator separator = new Separator(Orientation.HORIZONTAL);
                        card.getChildren().addAll(title, desc, notes, dueDate, complete, separator);
                        cardsToAdd.add(card);
                    }
                    milestonesMilestoneVB.getChildren().clear();
                    milestonesMilestoneVB.getChildren().addAll(cardsToAdd);
                    milestonesMilestonesRemaining.setText(String.format("Milestones remaining: %d", milestonesMilestoneVB.getChildren().size()));

                }
                break;

            default:
                if (lastLoaded != 0) {
                    //Date sysDate = new
                    dashboardWelcomeText.setText(String.format("Welcome, %s %s!", name[0], name[1]));
                    //dashboardCurrentWeek.setText();
                    int tasksRemaining = Student.GetTaskCountFromDB(currentUser);
                    dashboardTasksRemaining.setText(String.format("Tasks remaining: %d", tasksRemaining));
                    ArrayList<VBox> standard = new ArrayList<>();
                    ArrayList<VBox> upcoming = new ArrayList<>();
                    ArrayList<VBox> milestones = new ArrayList<>();
                    ArrayList<Task> tasks = Student.GetTasksFromDB(currentUser);
                    Map<String, long[]> ms = Milestone.GetMilestonesFromDBForUser(currentUser);
                    for (Task task : tasks) {
                        VBox card = new VBox();

                        Label taskName = new Label(task.description);
                        Label taskType = new Label(task.taskType.toString());
                        Label taskDueDate = new Label(task.dueDate);
                        Button gantt = new Button("View as gantt");
                        gantt.setStyle(String.valueOf(task.id));
                        Separator separator = new Separator(Orientation.HORIZONTAL);

                        EventHandler<ActionEvent> actionEventHandler = new EventHandler<>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                Button pressed = (Button) actionEvent.getSource();
                                showGantt(pressed.getStyle());
                            }
                        };
                        gantt.setOnAction(actionEventHandler);

                        card.getChildren().addAll(taskName, taskType, taskDueDate, gantt, separator);

                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        Date day;
                        long millis = 0;
                        try {
                            day = format.parse(task.dueDate.replaceAll("\"", ""));
                            millis = day.getTime();
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }

                        LocalDate currentDay = LocalDate.now();
                        LocalDate due = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalDate();

                        if (currentDay.compareTo(due) <= 0 && due.compareTo(currentDay.plusWeeks(1)) < 0) {
                            upcoming.add(card);
                        } else {
                            standard.add(card);
                        }
                    }

                    for (Map.Entry<String, long[]> milestone : ms.entrySet()) {
                        VBox card = new VBox();

                        String milestoneDate = "";

                        //Check if it is coursework or not
                        if (milestone.getValue()[0] == 0) {
                            //is exam
                            milestoneDate = Exam.getDateFromDB(Long.toString(milestone.getValue()[1]));

                        } else {
                            //is coursework
                            milestoneDate = Coursework.getDateFromDB(Long.toString(milestone.getValue()[0]));
                        }

                        Label title = new Label(milestone.getKey());
                        Label dueDate = new Label(milestoneDate);
                        Separator separator = new Separator(Orientation.HORIZONTAL);

                        card.getChildren().addAll(title, dueDate, separator);
                        milestones.add(card);
                    }

                    dashboardTasksRemainingAP.getChildren().clear();
                    dashboardTasksRemainingAP.getChildren().addAll(standard);
                    dashboardMilestonesVBox.getChildren().clear();
                    dashboardMilestonesVBox.getChildren().addAll(milestones);
                    dashboardDeadlinesVBox.getChildren().clear();
                    dashboardDeadlinesVBox.getChildren().addAll(upcoming);

                    int totalUpcoming = dashboardDeadlinesVBox.getChildren().size();
                    int totalStandard = dashboardTasksRemainingAP.getChildren().size();

                    totalStandard += totalUpcoming;

                    dashboardMilestoneText.setText(String.format("Milestones remaining: %d", ms.size()));
                    dashboardTasksRemaining.setText(String.format("Tasks remaining: %d", totalStandard));
                    dashboardDeadlinesText.setText(String.format("Deadlines this week: %d", totalUpcoming));
                }
                break;

        }
    }

    public void dashboardNewActivityButton(){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/Sia/View/dialogActivity.fxml"));
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Create new Activity");
            primaryStage.setScene(new Scene(root, 420, 470));
            primaryStage.show();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public void dashboardNewAssessmentButton(ActionEvent e){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/Sia/View/dialogAssessment.fxml"));
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Create new Assessment");
            primaryStage.setScene(new Scene(root, 420, 470));
            primaryStage.show();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public void loginLoginButton(ActionEvent e) throws Exception {
        if (params.length == 2){
            loginUsername.setText(params[0]);
            loginPassword.setText(params[1]);
        }
        if (StudyProfile.checkCredentials(loginUsername.getText(), loginPassword.getText())) {
            Parent root = FXMLLoader.load(getClass().getResource("/Sia/View/dashboard.fxml"));
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Dashboard");
            primaryStage.setScene(new Scene(root, 900, 600));
            primaryStage.show();

            currentUser = loginUsername.getText();
            name = Student.SelectNameFromDatabase(currentUser);
            Stage stage = (Stage) loginButton.getScene().getWindow();
            stage.close();
        }else{
            loginInfo.setText("Invalid Credentials");
        }
    }

    public void loginRegisterButton(ActionEvent e) {
        if (!loginButton.isVisible()) {
            if (!(StudyProfile.isExistingUser(loginUsername.getText())) && !(loginPassword.getText().equals(""))
                    && loginPassword.getText().equals(loginConPassword.getText())) {
                String dateFrom = loginDateFrom.getValue().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
                String dateTo = loginDateTo.getValue().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
                //Student + Study Profile Object Initializers

                try {
                    Student newStudent = new Student(loginUsername.getText(), loginForename.getText(), loginSurname.getText());
                    StudyProfile newProfile = new StudyProfile(newStudent, dateFrom, dateTo, loginPassword.getText());
                    loginInfo.setText("New student added");
                }catch (Exception exc){
                    loginInfo.setText("Error when creating student or study profile");
                    exc.printStackTrace();
                }

                loginButton.setLayoutX(359);
                loginRegisterButton.setLayoutX(359);
                loginButton.setVisible(true);
                loginConPassword.setVisible(false);
                loginConPasswordLabel.setVisible(false);
                loginDateFrom.setVisible(false);
                loginDateFromLabel.setVisible(false);
                loginDateTo.setVisible(false);
                loginDateToLabel.setVisible(false);
                loginForename.setVisible(false);
                loginForenameLabel.setVisible(false);
                loginSurname.setVisible(false);
                loginSurnameLabel.setVisible(false);
            }else {
                loginInfo.setText("Account already exists");
            }
        }
        else {
            loginButton.setLayoutX(526);
            loginRegisterButton.setLayoutX(526);
            loginButton.setVisible(false);
            loginConPasswordLabel.setVisible(true);
            loginConPassword.setVisible(true);
            loginDateFrom.setVisible(true);
            loginDateFromLabel.setVisible(true);
            loginDateTo.setVisible(true);
            loginDateToLabel.setVisible(true);
            loginForename.setVisible(true);
            loginForenameLabel.setVisible(true);
            loginSurname.setVisible(true);
            loginSurnameLabel.setVisible(true);
        }
    }

    public void dialogNewActivityButton() {
        try {
            String notes = dialogTaskNotes.getText();
            String taskDesc = dialogTaskCB.getValue().toString();
            Calendar cal = Calendar.getInstance();
            LocalDate ld = dialogTaskDate.getValue();
            cal.set(ld.getYear(), ld.getMonthValue(), ld.getDayOfMonth());
            long timeInMilis = cal.getTimeInMillis();
            if (dialogTaskCWRadio.isSelected()) {
                try {
                    String insertAct = String.format("INSERT INTO Activities(TaskID, Deadline, timeEstimate, timeSpent, Notes, Complete) " +
                            "VALUES ('%s', '%s', '%s', 0, '%s', 0)", Task.GetIDFromDesc(taskDesc), timeInMilis, timeInMilis, notes);
                    Connection conn = DataBase.Connect();
                    PreparedStatement ps = conn.prepareStatement(insertAct);
                    ps.executeUpdate();
                } catch (SQLException exc) {
                    exc.printStackTrace();
                }

                Stage stage = (Stage) dialogActivityButton.getScene().getWindow();
                stage.close();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return;
    }

    public void setDialogTaskModuleCode () {
        try {
            String sql = "SELECT ModuleCode FROM Modules ";
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            List<String> listOfModules = new ArrayList<String>();
            ObservableList<String> observableListOfModules = FXCollections.observableList(listOfModules);

            while (rs.next()) {
                observableListOfModules.add(rs.getString("ModuleCode"));
            }

            dialogTaskModuleCode.setItems(observableListOfModules);
            List<String> listOfTypes = new ArrayList<String>();
            ObservableList<String> observableListOfTypes = FXCollections.observableList(listOfTypes);
            String types[] = {"Study", "Revision", "Programming", "Writting"};
            observableListOfTypes.addAll(types);
            dialogTaskAssessmentCombo.setItems(observableListOfTypes);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setDialogActivityTasks () {
        try {
            //is coursework
            if (dialogTaskCWRadio.isSelected()) {
                String sql = String.format("SELECT Description FROM Tasks " +
                        "LEFT JOIN Courseworks WHERE Tasks.CourseworkID = " +
                        "Courseworks.CourseworkID AND " +
                        "Courseworks.CourseworkTitle = '%s'", dialogTaskAssessmentCombo.getValue());
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                List<String> listOfCW = new ArrayList<>();

                ObservableList<String> observableListOfCW = FXCollections.observableList(listOfCW);

                while (rs.next()) {
                    observableListOfCW.add(rs.getString("Description"));
                }

                dialogTaskCB.setItems(observableListOfCW);
            }
            //is exam
            else {
                String sql = String.format("SELECT Description FROM Tasks " +
                        "WHERE Tasks.ExamID = '%s'", dialogTaskAssessmentCombo.getValue());
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                List<String> listOfCW = new ArrayList<>();

                ObservableList<String> observableListOfCW = FXCollections.observableList(listOfCW);

                while (rs.next()) {
                    observableListOfCW.add(rs.getString("Description"));
                }

                dialogTaskCB.setItems(observableListOfCW);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDialogTaskAssessmentCombo() {
        if (dialogTaskCWRadio.isSelected()) {
            try {
                String sql = String.format("SELECT CourseworkTitle FROM Courseworks WHERE ModuleCode = '%s'", dialogTaskModuleCode.getValue());
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                List<String> listOfCW = new ArrayList<>();

                ObservableList<String> observableListOfCW = FXCollections.observableList(listOfCW);

                while (rs.next()) {
                    observableListOfCW.add(rs.getString("CourseworkTitle"));
                }

                dialogTaskAssessmentCombo.setItems(observableListOfCW);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                String sql = "SELECT ExamID FROM Exams WHERE Exams.ModuleCode = " + dialogTaskModuleCode.getValue();
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                List<String> listOfExams = new ArrayList<>();

                ObservableList<String> observableListOfExams = FXCollections.observableList(listOfExams);

                while (rs.next()) {
                    observableListOfExams.add(rs.getString("ExamID"));
                }

                dialogTaskAssessmentCombo.setItems(observableListOfExams);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void dialogTaskButton(ActionEvent e){
        String desc = dialogTaskDesc.getText();
        String notes = dialogTaskNotes.getText();
        String type = (String) dialogTaskType.getValue();
        String date = dialogTaskDate.getValue().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
        int Coursework = 0;
        int Exam;

        if (dialogTaskCWRadio.isSelected()){
            try {
                String sql = "SELECT CourseworkID FROM Courseworks WHERE Courseworks.CourseworkTitle = " + dialogTaskAssessmentCombo.getValue();
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                while(rs.next()){
                    Coursework = rs.getInt("CourseworkID");
                }

            } catch (SQLException exc){
                exc.printStackTrace();
            }
            Exam = 0;
        } else {
            Coursework = 0;
            Exam = Integer.parseInt((String) dialogTaskAssessmentCombo.getValue());
        }

        Task newTask = new Task(Task.TaskType.valueOf(type), Exam, Coursework,date, desc, notes);

        Stage stage = (Stage) dialogTaskButton.getScene().getWindow();
        stage.close();
    }

    public void dialogAssessmentButton(ActionEvent e){
        Coursework newCoursework;
        Exam newExam;

        String moduleCode = dialogAssessmentModuleCode.getText();
        String courseworkTitle = dialogAssessmentCWTitle.getText();
        float value = Float.parseFloat(dialogAssessmentValue.getText());
        String dateDue = dialogAssessmentDateDue.getValue().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));

        if(dialogAssessmentRadioCW.isSelected()){
            String dateSet = dialogAssessmentDateSet.getValue().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));

            newCoursework = new Coursework(courseworkTitle, dateSet, dateDue, moduleCode,value);
        }else{
            newExam = new Exam(dateDue, moduleCode, value);
        }
        Stage stage = (Stage) dialogAssessmentButton.getScene().getWindow();

        stage.close();
    }

    public void eventDialogAssessmentRadioCW(ActionEvent e){
        dialogAssessmentDateDueLabel.setText("Date Due");
        dialogAssessmentCWTitle.setDisable(false);
        dialogAssessmentDateSet.setDisable(false);
    }


    public void eventDialogAssessmentRadioExam(ActionEvent e){
        dialogAssessmentDateDueLabel.setText("Date of Exam");
        dialogAssessmentCWTitle.setDisable(true);
        dialogAssessmentDateSet.setDisable(true);
    }

    public void dialogMilestoneButton(ActionEvent e){
        String desc = dialogMilestoneDescription.getText();
        String notes = dialogMilestoneNotes.getText();
        String title = dialogMilestoneTitle.getText();
        int Coursework = 0;
        int Exam;

        if (dialogMilestoneCW.isSelected()){
            try {
                String sql = "SELECT CourseworkID FROM Courseworks WHERE Courseworks.CourseworkTitle = " + dialogMilestoneAssessment.getValue();
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                while(rs.next()){
                    Coursework = rs.getInt("CourseworkID");
                }

            } catch (SQLException exc){
                exc.printStackTrace();
            }
            Exam = 0;
        } else {
            Coursework = 0;
            Exam =  Integer.parseInt(dialogMilestoneAssessment.getValue().toString());
        }

        Milestone newMilestone = new Milestone(title, desc, notes, Coursework, Exam, currentUser);

        Stage stage = (Stage) dialogMilestoneButton.getScene().getWindow();
        stage.close();
    }

    public void setDialogMilestoneModuleCode(){
        try {
            String sql = "SELECT ModuleCode FROM Modules ";
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            List<String> listOfModules = new ArrayList<String>();
            ObservableList<String> observableListOfModules = FXCollections.observableList(listOfModules);

            while(rs.next()){
                observableListOfModules.add(rs.getString("ModuleCode"));
            }

            dialogMilestoneModuleCode.setItems(observableListOfModules);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void setDialogMilestoneAssessmentCombo() {
        if (dialogMilestoneCW.isSelected()) {
            try {
                String sql = "SELECT CourseworkTitle FROM Courseworks WHERE Courseworks.ModuleCode = " + dialogMilestoneModuleCode.getValue();
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                List<String> listOfCW = new ArrayList<>();

                ObservableList<String> observableListOfCW = FXCollections.observableList(listOfCW);

                while(rs.next()){
                    observableListOfCW.add(rs.getString("CourseworkTitle"));
                }

                dialogMilestoneAssessment.setItems(observableListOfCW);

            } catch (SQLException e){
                e.printStackTrace();
            }
        } else {
            try {
                String sql = String.format("SELECT ExamID FROM Exams WHERE Exams.ModuleCode = '%s' ", dialogMilestoneModuleCode.getValue());
                Connection conn = DataBase.Connect();
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                List<String> listOfExams = new ArrayList<>();

                ObservableList<String> observableListOfExams = FXCollections.observableList(listOfExams);

                while(rs.next()){
                    observableListOfExams.add(rs.getString("ExamID"));
                }

                dialogMilestoneAssessment.setItems(observableListOfExams);

            } catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    public void showGantt(String taskID){
        try {

            Stage primaryStage = new Stage();
            String[] name = Student.SelectNameFromDatabase(currentUser);
            primaryStage.setTitle(String.format("%s %s", name[0], name[1]));

            ArrayList<Activity> Activities = Task.GetActivitiesFromDB(taskID);
            ArrayList<Activity> Prerequisites = Task.GetPrereqFromDB(taskID);
            ArrayList<String> Milestones = Milestone.GetMilestonesFromDBUsingTaskID(taskID, currentUser);

            for (Activity act: Prerequisites) {
                Activities.add(act);
            }

            final DateAxis xAxis = new DateAxis();
            final CategoryAxis yAxis = new CategoryAxis();

            xAxis.setLabel("Date");
            xAxis.setTickLabelFill(Color.CHOCOLATE);

            yAxis.setLabel("Activity");
            yAxis.setTickLabelFill(Color.CHOCOLATE);
            yAxis.setTickLabelGap(10);

            LineChart<Date, String> chart = new LineChart<Date, String>(xAxis, yAxis);

            ArrayList<String> activityNames = new ArrayList();
            ObservableList<XYChart.Series<Date, String>> series = FXCollections.observableArrayList();

            Date endDateMilestone = null;

            for (Activity act: Activities){
                activityNames.add(act.notes);

                String dateToCheck = Coursework.GetDateSetForCWFromDB(Activity.ActivitiesCoursework(act));
                XYChart.Data startDate;

                ObservableList<XYChart.Data<Date, String>> complete = FXCollections.observableArrayList();
                ObservableList<XYChart.Data<Date, String>> incomplete = FXCollections.observableArrayList();

                if (!dateToCheck.equals("")){
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
                    Date set = sdf.parse(dateToCheck);
                    startDate = new XYChart.Data<Date, String>(set , act.notes);
                }else{
                    startDate = new XYChart.Data<Date, String>(new GregorianCalendar().getTime(), act.notes);
                }

                GregorianCalendar due = new GregorianCalendar();
                due.setTimeInMillis(act.dueDate);
                endDateMilestone = due.getTime();

                XYChart.Data endDate = new XYChart.Data<Date, String>(due.getTime(), act.notes);

                if (act.complete){
                    complete.add(startDate);
                    complete.add(endDate);
                    series.add(new XYChart.Series<>("Complete tasks", complete));
                }else {
                    incomplete.add(startDate);
                    incomplete.add(endDate);
                    series.add(new XYChart.Series<>("Incomplete Tasks", incomplete));
                }
            }

            if (Milestones != null) {
                for (String milestone : Milestones) {
                    ObservableList<XYChart.Data<Date, String>> milestoneData = FXCollections.observableArrayList();
                    Date startDateMilestone = new GregorianCalendar().getTime();
                    XYChart.Data startDate = new XYChart.Data<Date, String>(startDateMilestone, milestone);
                    XYChart.Data endDate = new XYChart.Data<Date, String>(endDateMilestone, milestone);
                    milestoneData.add(startDate);
                    milestoneData.add(endDate);
                    series.add(new XYChart.Series<>("Milestone", milestoneData));
                }
            }

            for (String milestone : Milestones){
                activityNames.add(milestone);
            }

            yAxis.setCategories(FXCollections.<String>observableArrayList(activityNames));

            chart.setData(series);

            chart.setTitle(String.format("%s", Task.GetDescriptionFromDB(taskID)));
            chart.setLegendVisible(true);

            chart.getStylesheets().add("Sia/View/ganttchart.css");

            Scene scene  = new Scene(chart,620,350);

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public void setParams(List<String> Params){
        params = Params.toArray(new String[0]);
    }
}