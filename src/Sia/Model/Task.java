package Sia.Model;

import java.sql.*;
import java.util.ArrayList;

public class Task {
    public int id;
    public ArrayList<Activity> activities = new ArrayList<>();
    public int courseworkID;
    public int examID;
    public TaskType taskType;
    public String dueDate;
    public String description;
    public String notes;
    public ArrayList<Activity> prerequisites = new ArrayList<>();

    public enum TaskType{
        Study, Revision, Programming, Writing;
    }

    public Task(TaskType taskType, int examID, int courseworkID, String dueDate, String description, String notes){
        this.taskType = taskType;
        this.dueDate = dueDate;
        this.description = description;
        this.notes = notes;
        this.examID = examID;
        this.courseworkID = courseworkID;

        InsertIntoDatabase();
    }

    public Task(int TaskID, TaskType taskType, int examID, int courseworkID, String dueDate, String description, String notes){
        this.id = TaskID;
        this.taskType = taskType;
        this.dueDate = dueDate;
        this.description = description;
        this.notes = notes;
        this.examID = examID;
        this.courseworkID = courseworkID;
    }

    public boolean InsertIntoDatabase() {
        try {
            String sql = String.format("INSERT INTO Tasks(CourseworkID, ExamID, TaskType, Deadline, Description, Notes) " +
                    "VALUES('%s','%s','%s','%s','%s','%s')", this.courseworkID, this.examID, this.taskType.name(),
                    this.dueDate, this.description, this.notes);
            System.out.println(this);

            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static ArrayList<Activity> GetActivitiesFromDB(String taskID){
        try {
            ArrayList<Activity> thisTasksActivities = new ArrayList<>();

            String Activities = String.format("SELECT Activities.Deadline, " +
                    "Activities.TimeEstimate, Activities.TimeSpent, " +
                    "Activities.Notes, Activities.Complete FROM Activities " +
                    "LEFT JOIN Tasks ON Tasks.TaskID = Activities.TaskID " +
                    "WHERE Activities.TaskID = %s", taskID);

            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(Activities);

            while(rs.next()){
                long deadline = rs.getLong(1);
                long timeEstimate = rs.getLong(2);
                long timeSpent = rs.getLong(3);
                String notes = rs.getString(4);
                boolean completed = false;

                switch(rs.getInt(5)){
                    case 1: completed = true;
                        break;
                    default: completed = false;
                        break;
                }
                thisTasksActivities.add(new Activity(0, taskID, deadline, timeEstimate, timeSpent, notes, completed));
            }
            return thisTasksActivities;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static boolean belongsToExam(String taskID) {
        boolean isForExam = false;
        try {

            String isExam = String.format("SELECT Tasks.ExamID WHERE " +
                    "Tasks.TaskID = %s ", taskID);

            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(isExam);

            while (rs.next()) {
                long isExamResponse = rs.getLong(1);
                if (isExamResponse > 0) {
                    isForExam = true;
                } else {
                    isForExam = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isForExam;
    }

    public static ArrayList<Activity> GetPrereqFromDB(String taskID){
        try {
            ArrayList<Activity> thisTasksActivities = new ArrayList<>();

            String Activities = String.format("SELECT Activities.Deadline, " +
                    "Activities.TimeEstimate, Activities.TimeSpent, " +
                    "Activities.Notes, Activities.Complete FROM Activities " +
                    "LEFT JOIN Prerequisites WHERE Prerequisites.TaskID = %s " +
                    "AND Prerequisites.ActivityID = Activities.ActivityID", taskID);

            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(Activities);

            while(rs.next()){
                long deadline = rs.getLong(1);
                long timeEstimate = rs.getLong(2);
                long timeSpent = rs.getLong(3);
                String notes = rs.getString(4);
                boolean completed = false;

                switch(rs.getInt(5)){
                    case 1: completed = true;
                        break;
                    default: completed = false;
                        break;
                }
                thisTasksActivities.add(new Activity(0, taskID, deadline, timeEstimate, timeSpent, notes, completed));
            }
            return thisTasksActivities;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Integer> getListOfActivities(Integer TaskID){
        try {
            ArrayList<Integer> Activities = new ArrayList<>();

            String sql = String.format("SELECT Activities.ActivityID FROM Activities " +
                    "WHERE Activities.TaskID = %s", TaskID);
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                //Code
                Activities.add(rs.getInt(1));
            }

            return Activities;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public static long GetIDFromDesc(String Desc) {
        try {
            String taskDesc = String.format("SELECT TaskID FROM " +
                    "Tasks WHERE Description = '%s'", Desc);

            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(taskDesc);
            long id = 0;

            while (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String GetDescriptionFromDB(String taskID) {
        try{
        String taskDesc = String.format("SELECT Description FROM " +
                "Tasks WHERE TaskID = %s", taskID);

        Connection conn = DataBase.Connect();

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(taskDesc);
        String Desc = "";

        while(rs.next()){
            Desc = rs.getString(1);
        }
        return Desc;
    }catch (Exception e){
        e.printStackTrace();
        return "Ooops an error occured!";
    }
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", activities=" + activities.toString() +
                ", taskType=" + taskType +
                ", dueDate=" + dueDate +
                ", description='" + description + '\'' +
                ", notes='" + notes + '\'' +
                ", prerequisite=" + prerequisites.toString() +
                '}';
    }
}
