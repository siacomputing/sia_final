package Sia.Model;

import java.sql.*;
import java.util.ArrayList;

public class Coursework extends Assessment {
    String title;
    String dateSet;
    String dateDue;
    String moduleCode;

    public Coursework(String title, String dateSet, String dateDue, String moduleCode, float value){
        this.title = title;
        this.dateSet = dateSet;
        this.dateDue = dateDue;
        this.module = new Module();
        this.module.SelectFromDatabase(moduleCode);
        this.value = value;

        InsertIntoDatabase();
    }

    public Coursework(String title, String dateSet, String dateDue, float value, String moduleCode){
        this.title = title;
        this.dateSet = dateSet;
        this.dateDue = dateDue;
        this.value = value;
        this.moduleCode = moduleCode;
    }

    public static ArrayList<Integer> getListOfTasks(Integer moduleCode){
        try {
            ArrayList<Integer> Tasks = new ArrayList<>();

            String sql = String.format("SELECT Tasks.TaskID FROM Tasks " +
                    "LEFT JOIN Courseworks WHERE Tasks.CourseworkID = Courseworks.CourseworkID " +
                    "AND Courseworks.ModuleCode = %s", moduleCode);
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                //Code
                Tasks.add(rs.getInt(1));
            }

            return Tasks;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean InsertIntoDatabase() {
        try {
            String sql = String.format("INSERT INTO Courseworks(ModuleCode, CourseworkTitle, Value, DateSet, DateDue)" +
                    "VALUES('%s', '%s', '%s', '%s', '%s')", this.module.moduleCode, this.title, value, dateSet, dateDue);

            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String GetDateSetForCWFromDB(int CourseworkID){
        try {
            String date = "";
            String Courseworks = String.format("SELECT Courseworks.DateSet FROM Courseworks " +
                   "WHERE Courseworks.CourseworkID = %s", CourseworkID);
            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(Courseworks);

            while (rs.next()) {
                date = rs.getString("Courseworks.DateSet");
            }
            return date;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getDateFromDB(String courseworkID) {
        String cwDate="";
        try {
            String isExam = String.format("SELECT Courseworks.DateDue FROM Courseworks " +
                    "WHERE Courseworks.CourseworkID = %s ", courseworkID);

            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(isExam);

            while (rs.next()) {
                cwDate = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cwDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDateSet() {
        return dateSet;
    }

    public String getDateDue() {
        return dateDue;
    }

    @Override
    public String toString() {
        return String.format("%s, %f, %s, %s, %s", title, value, dateSet, dateDue, moduleCode);
    }
}
