package Sia.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Module {
    public String moduleCode;
    public String moduleTitle;
    public ArrayList<Assessment> assessments;
    public String moduleOrganiser;
    public String organiserEmail;

    public Module(String moduleCode, String moduleTitle, ArrayList<Assessment> assessments, String moduleOrganiser, String organiserEmail){
        this.moduleCode = moduleCode;
        this.moduleTitle = moduleTitle;
        this.assessments = assessments;
        this.moduleOrganiser = moduleOrganiser;
        this.organiserEmail = organiserEmail;
    }

    public Module(){
        this.assessments = new ArrayList<>();
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public String getModuleTitle() {
        return moduleTitle;
    }

    public Assessment getAssessment(int i) {
        return assessments.get(i);
    }

    public void addAssessment(Assessment assessment) {
        this.assessments.add(assessment);
    }

    public String getModuleOrganiser() {
        return moduleOrganiser;
    }

    public String getOrganiserEmail() {
        return organiserEmail;
    }

    public static String getModuleName(Integer ModuleID){
        try {
            String title = "";

            String sql = String.format("SELECT Modules.ModuleTitle FROM Modules " +
                    "WHERE Modules.ModuleCode = %s", ModuleID);
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                //Code
                title = rs.getString(1);
            }

            return title;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Integer> getListOfEnrolledModule(String CurrentUser){
        try {
            ArrayList<Integer> modules = new ArrayList<>();

            String sql = String.format("SELECT Enrollment.ModuleCode FROM Enrollment " +
                    "WHERE Enrollment.StudyProfileID = %s", Student.GetStudyProfileID(CurrentUser));
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                //Code
                modules.add(rs.getInt(1));
            }

            return modules;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean SelectFromDatabase(String ModuleCode){
        try {
            String sql = "SELECT * FROM Modules WHERE ModuleCode = " + ModuleCode;
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                //Code
                this.moduleCode = rs.getString("ModuleCode");
                //Title
                this.moduleTitle = rs.getString("ModuleTitle");
                //Organiser
                this.moduleOrganiser = rs.getString("ModuleOrganiser");
                //Email for organiser
                this.organiserEmail = rs.getString("OrganiserEmail");
                //TODO MAKE ARRAY LIST FILL WITH ASSESSMENTS
                //assessments = new ArrayList<>();

            }

            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public String toString() {
        return "Module{" +
                "moduleCode='" + moduleCode + '\'' +
                ", moduleTitle='" + moduleTitle + '\'' +
                ", assessments=" + assessments.toString() +
                ", moduleOrganiser='" + moduleOrganiser + '\'' +
                ", organiserEmail='" + organiserEmail + '\'' +
                '}';
    }
}
