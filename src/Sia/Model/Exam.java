package Sia.Model;

import java.sql.*;
import java.util.ArrayList;

public class Exam extends Assessment {
    String date;
    String moduleCode;

    public Exam(String date, String module, float value) {
        this.date = date;
        this.module = new Module();
        this.module.SelectFromDatabase(module);
        this.value = value;

        InsertIntoDatabase();
    }

    public Exam(String date, float value, String moduleCode) {
        this.date = date;
        this.value = value;
        this.moduleCode = moduleCode;
    }

    public boolean InsertIntoDatabase() {
        try {

            String sql = String.format("INSERT INTO Exams(ModuleCode, Value, Date)" +
                    "VALUES('%s', '%s', '%s')", this.module.moduleCode, value, date);

            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static ArrayList<Integer> getListOfTasks(Integer moduleCode){
        try {
            ArrayList<Integer> Tasks = new ArrayList<>();

            String sql = String.format("SELECT Tasks.TaskID FROM Tasks " +
                    "LEFT JOIN Exams WHERE Tasks.ExamID = Exams.ExamID " +
                    "AND Exams.ModuleCode = %s", moduleCode);
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                //Code
                Tasks.add(rs.getInt(1));
            }

            return Tasks;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public static String getDateFromDB(String examID) {
        String courseworkDate="";
        try {
            String isExam = String.format("SELECT Exams.Date FROM Exams " +
                    "WHERE Exams.ExamID = %s ", examID);

            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(isExam);

            while (rs.next()) {
                courseworkDate = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courseworkDate;
    }

    @Override
    public String toString() {
        return String.format("%f, %s, %s", value, date, moduleCode);
    }
}
