package Sia.Model;

import java.sql.*;

public class Activity {
    public int id;
    public String task;
    public long dueDate;
    public long timeEstimate;
    public long timeSpent;
    public String notes;
    public boolean complete;

    public Activity(int id, String task, long dueDate, long timeEstimate, long timeSpent, String notes, boolean complete){
        this.id = id;
        this.task = task;
        this.dueDate = dueDate;
        this.timeEstimate = timeEstimate;
        this.timeSpent = timeSpent;
        this.notes = notes;
        this.complete = complete;
    }

    public Activity(String task, long dueDate, long timeEstimate, String notes){
        this.id = id;
        this.task = task;
        this.dueDate = dueDate;
        this.timeEstimate = timeEstimate;
        this.timeSpent = timeSpent;
        this.notes = notes;
        this.complete = complete;


    }

    public int getId() {
        return id;
    }

    public long getTimeEstimate() {
        return timeEstimate;
    }

    public long getTimeSpent() {
        return timeSpent;
    }

    public String getNotes() {
        return notes;
    }

    public void addNotes(String notes) {
        this.notes.concat(notes);
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public static int ActivitiesCoursework(Activity act) {
        try {
            int CourseworkID = 0;
            String Courseworks = String.format("SELECT Tasks.CourseworkID FROM Activities " +
                    "LEFT JOIN Tasks ON Activities.TaskID = Tasks.TaskID " +
                    "WHERE Activities.TaskID = %s AND Tasks.CourseworkID != 0", act.id);
            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(Courseworks);

            while (rs.next()) {
                CourseworkID = rs.getInt("Tasks.CourseworkID");
            }
            return CourseworkID;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int getTimeTaken(Integer ActivityID){
        try {
            int timeSpent = 0;

            String sql = String.format("SELECT Activities.TimeSpent FROM Activities " +
                    "WHERE Activities.ActivityID = %s", ActivityID);
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                //Code
                timeSpent = rs.getInt(1);
            }

            return timeSpent;
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", task=" + task +
                ", dueDate=" + dueDate +
                ", timeEstimate=" + timeEstimate +
                ", timeSpent=" + timeSpent +
                ", notes='" + notes + '\'' +
                ", complete=" + complete +
                '}';
    }
}

