package Sia.Model;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBase {
    private static final String sqliteDB = "jdbc:sqlite:SIADatabase.db";

    public static Connection Connect() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection(sqliteDB);
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println(e);
            System.exit(29035);
        }
        return null;
    }

    public boolean NewDatabase(String newDB){
        FileInputStream fis = null;
        InputStreamReader isr = null;
        String SQL = "ATTACH DATABASE 'other.db' AS other; INSERT INTO main.Students SELECT * FROM other.Students;" +
                "INSERT INTO main.Students SELECT * FROM other.Students;" +
                "INSERT INTO main.Exams SELECT * FROM other.Exams;" +
                "INSERT INTO main.Courseworks SELECT * FROM other.Courseworks;" +
                "INSERT INTO main.StudyProfiles SELECT * FROM other.StudyProfile;" +
                "INSERT INTO main.Tasks SELECT * FROM other.Tasks;" +
                "INSERT INTO main.Modules SELECT * FROM other.Modules;" +
                "INSERT INTO main.Milestones SELECT * FROM other.Milestones;" +
                "INSERT INTO main.Enrollment SELECT * FROM other.Enrollment;" +
                "INSERT INTO main.Activities SELECT * FROM other.Activities;" +
                "INSERT INTO main.Prerequisites SELECT * FROM other.Prerequisites;";
        return false;
    }
}
