package Sia.Model;

import java.sql.*;
import java.util.ArrayList;

public class StudyProfile {
    String id;
    String password;
    Student student;
    String startDate;
    String endDate;
    ArrayList<Module> modules;
    ArrayList<Milestone> milestones;

    public StudyProfile(Student studentParsed, String startDate, String endDate, String password) {
        id = studentParsed.getId();
        student = studentParsed;
        this.startDate = startDate;
        this.endDate = endDate;
        this.password = password;

        //Add to Database
        InsertIntoDatabase();
    }

    public StudyProfile(String id, Student student, String startDate, String endDate, ArrayList<Module> modules,
                        ArrayList<Milestone> milestones) {
        this.id = id;
        this.student = student;
        this.startDate = startDate;
        this.endDate = endDate;
        this.modules = modules;
        this.milestones = milestones;
    }

    public String getId() {
        return id;
    }

    public Student getStudent() {
        return student;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public Module getModule(int i) {
        return modules.get(i);
    }

    public void addModule(Module module) {
        this.modules.add(module);
    }

    public Milestone getMilestone(int i) {
        return milestones.get(i);
    }

    public void addMilestone(Milestone milestone) {
        this.milestones.add(milestone);
    }

    @Override
    public String toString() {
        return "StudyProfile{" +
                "id=" + id +
                ", student=" + student +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", modules=" + modules.toString() +
                ", milestones=" + milestones.toString() +
                '}';
    }

    /**
     * Insert a new row into the Study Profile table
     */
    public boolean InsertIntoDatabase() {
        try {
            String sql = String.format("INSERT INTO StudyProfiles(StudentID, DateStart, DateEnd, Password)" +
                    "VALUES('%s','%s','%s','%s')", id, startDate, endDate, password);

            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Select a record from the Study Profile table
     */
    public boolean SelectFromDatabase(String StudentID){
        try {
            String sql = "SELECT * FROM StudyProfiles WHERE StudentID = " + StudentID;
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery(sql);

            while(rs.next()){
                //Study Profile ID
                this.id = Integer.toString(rs.getInt("StudyProfileID"));
                //Student
                this.student = new Student(rs.getString("StudentID") + "\t");
            }
            return true;
        } catch (SQLException e){
            return false;
        }
    }

    /**
     * Update the record within the Database
     */
    public boolean UpdateDatabase() throws SQLException {
        try {
            String sql = String.format("UPDATE StudyProfile   " +
                    "SET StudentID = %s, DateStart = %s, DateEnd = %s, Modules = %s, Milestones = %s, Password = %s  " +
                    "WHERE StudyProfileID = %s",
                    student.getId(), startDate, endDate, modules, milestones, password, this.id);

            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * Works out if the StudentID is already used
     */
    public static boolean isExistingUser(String StudentID){
        try {
            int count = 0;
            String sql = String.format("SELECT COUNT (StudentID) FROM StudyProfiles WHERE StudentID = %s", StudentID);
            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while(rs.next()){
                count = rs.getInt("COUNT (StudentID)");;
            }

            return count > 0;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    /**
     * Checks the credentials of a user logging in
     */
    public static boolean checkCredentials(String StudentID, String password){
        try {
            String dbPassword = "";
            String sql = String.format("SELECT Password FROM StudyProfiles WHERE StudentID = %s", StudentID);
            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while(rs.next()){
                dbPassword = rs.getString("Password");;
            }

            return dbPassword.equals(password);

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
