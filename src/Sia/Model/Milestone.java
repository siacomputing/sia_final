package Sia.Model;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Milestone {
    public String coursworkTitle;
    public String moduleCode;
    public int id;
    public String title;
    public String description;
    public String notes;
    public int CourseworkID;
    public int ExamID;


    public Milestone(String title, String description, String notes, int courseworkID, int examID, String StudentID){
        this.title = title;
        this.description = description;
        this.notes = notes;
        this.CourseworkID = courseworkID;
        this.ExamID = examID;

        InsertIntoDatabase(StudentID);
    }

    public Milestone(String title, String description, String notes, String moduleCode, String courseworkTitle){
        this.title = title;
        this.description = description;
        this.notes = notes;
        this.moduleCode = moduleCode;
        this.coursworkTitle = courseworkTitle;
    }

    public boolean InsertIntoDatabase(String studentID) {
        try {
            String sql = String.format("INSERT INTO Milestones(Title, Description, Notes, CourseworkID, ExamID, StudyProfileID, Completed) " +
                            "VALUES('%s','%s','%s','%s','%s','%s',0)", this.title, this.description, this.notes,
                    this.CourseworkID, this.ExamID, Student.GetStudyProfileID(studentID));

            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String[] MilestoneData(String title){
        String[] data = new String[2];

        try{
            String isComplete = String.format("SELECT Milestones.Description, Milestones.Notes FROM Milestones " +
                    "WHERE Milestones.Title = '%s'", title);

            Connection conn = DataBase.Connect();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(isComplete);

            while(rs.next()) {
                data[0]  = rs.getString(1);
                data[1] = rs.getString(2);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }


    public static Map<String,long[]> GetMilestonesFromDBForUser(String userID){
        String studyProfileID = Student.GetStudyProfileID(userID);

        try {
            Map<String,long[]> milestoneList = new HashMap<>();
            String StudyProfileID = Student.GetStudyProfileID(userID);

            String Milestones = String.format("SELECT Milestones.Title, Milestones.CourseworkID," +
                    "Milestones.ExamID FROM " +
                    "Milestones WHERE Milestones.StudyProfileId = %s", StudyProfileID);

            Connection conn = DataBase.Connect();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(Milestones);

            while (rs.next()) {
                String milestone = rs.getString(1);
                long cw = rs.getLong(2);
                long exam = rs.getLong(3);
                long temp[] = new long[]{cw,exam};
                milestoneList.put(milestone,temp);
            }
            return milestoneList;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static void updateMilestone(String title, boolean isTicked){
        try{
            long res = 0;

            if (isTicked){ res = 1; }
            String isComplete = String.format("UPDATE Milestones SET Completed = %s " +
                    "WHERE Milestones.Title = '%s'", res, title);

            Connection conn = DataBase.Connect();
            Statement st = conn.createStatement();
            st.executeUpdate(isComplete);
            return;

        }catch (Exception e){
            e.printStackTrace();
            return;
        }
    }


    public static boolean isMilestoneComplete(String title){
        try{
            String isComplete = String.format("SELECT Milestones.Completed FROM Milestones " +
                    "WHERE Milestones.Title = '%s'", title);

            Connection conn = DataBase.Connect();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(isComplete);

            while(rs.next()) {
                Long comp = rs.getLong(1);
                conn.close();
                return (comp) == 1;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public static ArrayList<String> GetMilestonesFromDBUsingTaskID(String taskID, String userID){
        try {
            ArrayList<String> thisTasksMilestones = new ArrayList<>();
            Long CWID = null;
            Long ExamID = null;
            Boolean isExam = false;
            String StudyProfileID = Student.GetStudyProfileID(userID);

            String examOrCW = String.format("SELECT Tasks.CourseworkID, Tasks.ExamID FROM " +
                                            "Tasks WHERE Tasks.TaskID = %s", taskID);

            Connection conn = DataBase.Connect();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(examOrCW);

            while(rs.next()){
                CWID = rs.getLong(1);
                ExamID = rs.getLong(2);

                if (ExamID > 0 ){
                    isExam = true;
                }else {
                    isExam = false;
                }

            }

            if (isExam){
                String examMilestone = String.format("SELECT Milestones.Title FROM Milestones" +
                                                     "WHERE Milestones.ExamID = %s AND Milestones.StudyProfileID = %s ", ExamID, StudyProfileID);

                conn = DataBase.Connect();
                st = conn.createStatement();
                rs = st.executeQuery(examMilestone);

                while(rs.next()) {
                    String title = rs.getString(1);
                    thisTasksMilestones.add(title);
                }

                } else{

                String cwMilestone = String.format("SELECT Milestones.Title FROM Milestones " +
                        "WHERE Milestones.CourseworkID = %s AND Milestones.StudyProfileID = %s ", CWID, StudyProfileID);

                conn = DataBase.Connect();
                st = conn.createStatement();
                rs = st.executeQuery(cwMilestone);

                while(rs.next()) {
                    String title = rs.getString(1);
                    thisTasksMilestones.add(title);
                }
            }

            return thisTasksMilestones;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getNotes() {
        return notes;
    }

    public void addNotes(String notes) {
        this.notes.concat(notes);
    }

    @Override
    public String toString() {
        return "Milestone{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
