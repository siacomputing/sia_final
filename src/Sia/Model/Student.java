package Sia.Model;

import java.sql.*;
import java.util.ArrayList;

public class Student {
    String id;
    String forename;
    String surname;

    public Student(String id){
        this.id = id;
    }

    public Student(String ID, String Forename, String Surname){
        this.id = ID;
        this.setForename(Forename);
        this.surname = Surname;

        InsertIntoDatabase();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id; }

    public String getForename() {
        return forename;
    }

    public boolean setForename(String forename) {
        if (forename.length() > 0){
            this.forename = forename;
            return true;
        }
        return false;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", forename='" + forename + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    /**
     * Insert a new row into the Study Profile table
     */
    public boolean InsertIntoDatabase() {
        try {
            String sql = String.format("INSERT INTO Students(StudentID, Forename, Surname)" +
                    "VALUES('%s', '%s', '%s')", this.id, this.forename, this.surname);

            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * Select a record from the Study Profile table
     */
    public boolean SelectFromDatabase(String StudentID){
        try {
            String sql = "SELECT * FROM StudyProfiles WHERE StudentID = " + StudentID;
            Connection conn = DataBase.Connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery(sql);

            while(rs.next()){
                //Study Profile ID
                this.id = Integer.toString(rs.getInt("StudyProfileID"));
                //Student
               // this.student = new Student(rs.getString("StudentID") + "\t");

            }

            return true;
        } catch (SQLException e){
            return false;
        }
    }

    /**
     * Update the record within the Database
     */
    public boolean UpdateDatabase() throws SQLException {
     //   try {
//            String sql = String.format("UPDATE StudyProfile   " +
//                            "SET StudentID = %s, DateStart = %d, DateEnd = %d, Modules = %s, Milestones = %s, Password = %s  " +
//                            "WHERE StudyProfileID = %s",
//                    student.getId(), startDate, endDate, modules, milestones, password, this.id);

            Connection conn = DataBase.Connect();
//            PreparedStatement ps = conn.prepareStatement(sql);
  //          ps.executeUpdate();
            return true;
    //    } catch (SQLException e) {
    //        return false;
      //  }
    }

    /**
     * Select a record from the Study Profile table
     */
    public static boolean isExistingUser(String StudentID){
        try {
            int count = 0;
            String sql = String.format("SELECT COUNT (StudentID) FROM StudyProfile WHERE StudentID = %s", StudentID);
            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while(rs.next()){
                count = rs.getInt("COUNT (StudentID)");;
            }

            return count > 0;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static String[] SelectNameFromDatabase(String StudentID){
        try {
            String sql = String.format("SELECT Forename, Surname FROM Students WHERE StudentID = %s", StudentID);
            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            String name[] = new String[2]; String forename = ""; String surname = "";

            while(rs.next()){
                forename = rs.getString("Forename");
                surname = rs.getString("Surname");
            }

            name[0] = forename;
            name[1] = surname;

            return name;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static int GetTaskCountFromDB(String StudentID){
        try {
            int courseworkTaskCount = 0;
            int examTaskCount = 0;
            String Courseworks = String.format("SELECT count(Tasks.TaskID) FROM Students " +
                    "LEFT JOIN StudyProfiles ON Students.StudentID = StudyProfiles.StudentID " +
                    "LEFT JOIN Enrollment ON StudyProfiles.StudyProfileID = Enrollment.StudyProfileID " +
                    "LEFT JOIN Courseworks ON Enrollment.ModuleCode = Courseworks.ModuleCode " +
                    "LEFT JOIN Tasks ON Courseworks.CourseworkID = Tasks.CourseworkID " +
                    "WHERE Students.StudentID = %s AND Tasks.CourseworkID != 0", StudentID);

            String Exams = String.format("SELECT count(Tasks.TaskID) FROM Students\n" +
                    "LEFT JOIN StudyProfiles ON Students.StudentID = StudyProfiles.StudentID\n" +
                    "LEFT JOIN Enrollment ON StudyProfiles.StudyProfileID = Enrollment.StudyProfileID\n" +
                    "LEFT JOIN Exams ON Enrollment.ModuleCode = Exams.ModuleCode \n" +
                    "LEFT JOIN Tasks ON Exams.ExamID = Tasks.ExamID\n" +
                    "WHERE Students.StudentID = %s AND Tasks.ExamID != 0", StudentID);

            Connection conn = DataBase.Connect();

            Statement st1 = conn.createStatement();
            Statement st2 = conn.createStatement();
            ResultSet rs1 = st1.executeQuery(Courseworks);
            ResultSet rs2 = st2.executeQuery(Exams);

            while(rs1.next() ){
                courseworkTaskCount = rs1.getInt("count(Tasks.TaskID)");
            }
            while(rs2.next()){
                examTaskCount = rs2.getInt("count(Tasks.TaskID)");
            }

            int total = courseworkTaskCount + examTaskCount;

            return total;
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public static ArrayList<Assessment> GetAssessmentsFromDB(String currentUser) {
        try{
           ArrayList<Assessment> assessments = new ArrayList<>();
            String cwTasks = String.format("SELECT Courseworks.CourseworkTitle, Courseworks.Value, Courseworks.DateSet, " +
                "Courseworks.DateDue, Courseworks.ModuleCode FROM Courseworks " +
                "LEFT JOIN Enrollment ON Enrollment.ModuleCode = Courseworks.ModuleCode "+
                "LEFT JOIN StudyProfiles ON StudyProfiles.StudyProfileID = Enrollment.StudyProfileID "+
                "LEFT JOIN Students ON Students.StudentID = StudyProfiles.StudentID "+
                "WHERE Students.StudentID = '%s'", currentUser);

            String examTasks = String.format("SELECT Exams.Value, Exams.Date, Exams.ModuleCode FROM Exams " +
                    "LEFT JOIN Enrollment ON Enrollment.ModuleCode = Exams.ModuleCode " +
                    "LEFT JOIN StudyProfiles ON StudyProfiles.StudyProfileID = Enrollment.StudyProfileID " +
                    "LEFT JOIN Students ON Students.StudentID = StudyProfiles.StudentID "+
                    "WHERE Students.StudentID = '%s'", currentUser);


        Connection conn = DataBase.Connect();

        Statement st1 = conn.createStatement();
        ResultSet rs1 = st1.executeQuery(cwTasks);

        while(rs1.next()){
            String title = rs1.getString(1);
            float value = rs1.getFloat(2);
            String dateset = rs1.getString(3);
            String datedue = rs1.getString(4);
            String module = rs1.getString(5);
            assessments.add(new Coursework(title, dateset, datedue, value, module));
        }

        rs1 = st1.executeQuery(examTasks);
        while(rs1.next()){
            float value = rs1.getFloat(1);
            String date = rs1.getString(2);
            String module = rs1.getString(3);
            assessments.add(new Exam(date, value, module));
        }
            return assessments;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Task> GetTasksFromDB(String studentID){
        try {
            ArrayList<Task> tasks = new ArrayList<>();

            String cwTasks = String.format("SELECT Tasks.TaskID, Tasks.CourseworkID, Tasks.ExamID, Tasks.TaskType, Tasks.Deadline, Tasks.Description, Tasks.Notes FROM Students\n" +
                    "LEFT JOIN StudyProfiles ON Students.StudentID = StudyProfiles.StudentID\n" +
                    "LEFT JOIN Enrollment ON StudyProfiles.StudyProfileID = Enrollment.StudyProfileID\n" +
                    "LEFT JOIN Courseworks ON Enrollment.ModuleCode = Courseworks.ModuleCode \n" +
                    "LEFT JOIN Tasks ON Courseworks.CourseworkID = Tasks.CourseworkID\n" +
                    "WHERE Students.StudentID = %s AND Tasks.CourseworkID != 0", studentID);

            String examTasks = String.format("SELECT Tasks.TaskID, Tasks.CourseworkID, Tasks.ExamID, Tasks.TaskType, Tasks.Deadline, Tasks.Description, Tasks.Notes FROM Students\n" +
                    "LEFT JOIN StudyProfiles ON Students.StudentID = StudyProfiles.StudentID\n" +
                    "LEFT JOIN Enrollment ON StudyProfiles.StudyProfileID = Enrollment.StudyProfileID\n" +
                    "LEFT JOIN Exams ON Enrollment.ModuleCode = Exams.ModuleCode \n" +
                    "LEFT JOIN Tasks ON Exams.ExamID = Tasks.ExamID\n" +
                    "WHERE Students.StudentID = %s AND Tasks.ExamID != 0", studentID);


            Connection conn = DataBase.Connect();

            Statement st1 = conn.createStatement();
            ResultSet rs1 = st1.executeQuery(cwTasks);

            while(rs1.next()){
                int taskID = rs1.getInt("TaskID");
                int courseworkID = rs1.getInt("CourseworkID");
                int examID = rs1.getInt("ExamID");
                String type = rs1.getString("TaskType");
                String deadline = rs1.getString("Deadline");
                String description = rs1.getString("Description");
                String notes = rs1.getString("Notes");
                tasks.add(new Task(taskID, Task.TaskType.valueOf(type), examID, courseworkID, deadline, description, notes));
            }

            rs1 = st1.executeQuery(examTasks);
            while(rs1.next()){
                int taskID = rs1.getInt("TaskID");
                int courseworkID = rs1.getInt("CourseworkID");
                int examID = rs1.getInt("ExamID");
                String type = rs1.getString("TaskType");
                String deadline = rs1.getString("Deadline");
                String description = rs1.getString("Description");
                String notes = rs1.getString("Notes");
                tasks.add(new Task(taskID, Task.TaskType.valueOf(type), examID, courseworkID, deadline, description, notes));
            }
            return tasks;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


    public static ArrayList<Milestone> GetMilestonesFromDB(String currrentUser){
        try {
            ArrayList<Milestone> milestones = new ArrayList<>();

            String cwMilestone = String.format("SELECT Modules.ModuleCode, Courseworks.CourseworkTitle,  Milestones.Title, " +
                    "Milestones.Description, Milestones.Notes FROM Students " +
                    "LEFT JOIN StudyProfiles ON Students.StudentID = StudyProfiles.StudentID " +
                    "LEFT JOIN Milestones ON StudyProfiles.StudyProfileID = Milestones.StudyProfileID " +
                    "LEFT JOIN Courseworks ON Milestones.CourseworkID = Courseworks.CourseworkID " +
                    "LEFT JOIN Modules ON Courseworks.ModuleCode = Modules.ModuleCode " +
                    "WHERE Students.StudentID = %s AND Milestones.CourseworkID != 0", currrentUser);

            String examMilestone = String.format("SELECT Modules.ModuleCode, Milestones.Title, " +
                    "Milestones.Description, Milestones.Notes FROM Students " +
                    "LEFT JOIN StudyProfiles ON Students.StudentID = StudyProfiles.StudentID " +
                    "LEFT JOIN Milestones ON StudyProfiles.StudyProfileID = Milestones.StudyProfileID " +
                    "LEFT JOIN Exams ON Milestones.ExamID = Exams.ExamID " +
                    "LEFT JOIN Modules ON Exams.ModuleCode = Modules.ModuleCode " +
                    "WHERE Students.StudentID = %s AND Milestones.ExamID != 0", currrentUser);

            Connection conn = DataBase.Connect();

            Statement st1 = conn.createStatement();
            ResultSet rs1 = st1.executeQuery(cwMilestone);

            while(rs1.next()){
                String module = rs1.getString(1);
                String cwTitle = rs1.getString(2);
                String title = rs1.getString(3);
                String description = rs1.getString(4);
                String notes = rs1.getString(5);
                milestones.add(new Milestone(title, description, notes, module, cwTitle));
            }

            rs1 = st1.executeQuery(examMilestone);
            while(rs1.next()){
                String module = rs1.getString(1);
                String title = rs1.getString(2);
                String description = rs1.getString(3);
                String notes = rs1.getString(4);
                milestones.add(new Milestone(title, description, notes, module, ""));
            }


            return milestones;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }



    public static String GetStudyProfileID(String StudentID){
        try {
            String id = "";
            String sql = String.format("SELECT StudyProfiles.StudyProfileID FROM Students " +
                    "LEFT JOIN StudyProfiles ON Students.StudentID = StudyProfiles.StudentID " +
                    "WHERE Students.StudentID = %s", StudentID);
            Connection conn = DataBase.Connect();

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while(rs.next()){
                id = rs.getString(1);
            }

            return id;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }




}



