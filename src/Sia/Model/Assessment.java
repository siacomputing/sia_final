package Sia.Model;

import java.util.ArrayList;

public abstract class Assessment {
    Module module;
    float value;
    Task task;
    ArrayList<Milestone> milestones = new ArrayList<>();

    public Module getModule() {
        return module;
    }

    public float getValue() {
        return value;
    }
}
